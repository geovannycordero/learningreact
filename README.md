# React Web Developer Course

## From Udemy

Started at: October 30, 2019

**Motivation:** First time with React. Part of the training in the Apprentice Program at Pernix. One of the first web frameworks that I learn.

**Scope:** Indecision application. The user has an unordered list of tasks and the app choose on for him. The user can add or remove task. In the _playground_ folder there are another files to learn specific topics about the framework or JavaScript.

**Lessons Learned:** .

**Credits:** to @elmermbp @jgonzalezPernix for review the code.

# Setup
Step by step to run the project.

### Install yarn
```bash
sudo npm i -g yarn
```
### Install live-server
```bash
sudo npm i -g live-server
```

### Create the package.json file
```bash
yarn init
```

### Install babel-cli
```bash
sudo npm i -g babel-cli
```

### Add babel dependencies to the project
```bash
yarn add babel-preset-react babel-preset-env
```

### Translate the JSX code to JS
```bash
babel src/app.js --out-file=public/scripts/app.js --presets=env,react
```

### Translate the JSX code to JS and keep it running to automatically change the destination file
```bash
babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch
```

### Run the local server
```bash
live-server public
```

The server is running locally at http://127.0.0.1:8080 and the web browser should open a new tab automatically.
