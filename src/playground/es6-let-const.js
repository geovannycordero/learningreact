var nameVar = 'Geovanny';
var nameVar = 'Alberto';
console.log('nameVar:', nameVar);

let nameLet = 'Kasandra';
nameLet = 'Viviana';
console.log('nameLet:', nameLet);

const nameConst = 'San Luis';
console.log('nameConst:', nameConst);

// function scope
function getPetName() {
  var petName = 'Chacho';
  return petName;
}

const petName = getPetName();
console.log('petName:', petName);

//block scope
var fullName = 'Geovanny Cordero';
let firstName;

if(fullName) {
  firstName = fullName.split(' ')[0];
  console.log('firstName: ', firstName);
}

console.log('firstName: ', firstName);
