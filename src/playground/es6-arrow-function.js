const square = function (x) {
  return x * x;
};

console.log('Arrow', square(8));

const squareArrow = x => x * x;

console.log('squareArrow', squareArrow(8));

const getFisrtName = fullName => fullName.split(' ')[0];

console.log('getFisrtName', getFisrtName('Geovanny Cordero'));
