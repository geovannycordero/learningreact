const appInfo = {
  title: 'This is JSX from app.js',
  subtitle: 'This is some information',
  options: ['one', 'two', 'three']
}

const template = (
  <div>
    {appInfo.title && <h1>{appInfo.title}</h1>}
    <p>{appInfo.subtitle}</p>
    <p>{0 < appInfo.options.length ? 'Here are your options' : 'No options'}</p>
    <ol>
      <li>Item One</li>
      <li>Item Two</li>
    </ol>
  </div>
);

const user = {
  name: 'Geovanny',
  age: 21,
  location: 'San Jose'
}

function getLocation(location) {
  return location ? <p>Location: {location}</p> : undefined;
}

const templateTwo = (
  <div>
    <h1>{user.name ? user.name : 'Anonymous'}</h1>
    {(user.age && 18 <= user.age) && <p>Age: {user.age}</p>}
    {getLocation(user.location)}
  </div>
);
