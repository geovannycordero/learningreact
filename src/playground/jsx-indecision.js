const appInfo = {
  title: 'This is JSX from app.js',
  subtitle: 'This is some information',
  options: []
}

const onFormSubmit = (e) => {
  e.preventDefault();

  const option = e.target.elements.option.value;
  if(option) {
    appInfo.options.push(option);
    e.target.elements.option.value = '';
  }

  renderOptionsChanges();
};

const removeAllOptions = () => {
  appInfo.options = [];
  renderOptionsChanges();
};

const onMakeDecision = () => {
  const randomNum = Math.floor(Math.random() * appInfo.options.length);
  const optionSelected = appInfo.options[randomNum];
  alert(optionSelected);
}

let appRoot = document.getElementById('app');

const numbers = [67, 45, 2, 99, 6991];

const renderOptionsChanges = () => {
  const template = (
    <div>
      {appInfo.title && <h1>{appInfo.title}</h1>}
      <p>{appInfo.subtitle}</p>
      <p>{0 < appInfo.options.length ? 'Here are your options' : 'No options'}</p>
      <button disabled={appInfo.options.length === 0} onClick={onMakeDecision}>What should I do?</button>
      <button disabled={appInfo.options.length === 0} onClick={removeAllOptions}>Remove All</button>
      <ol>
        {appInfo.options.map(option => <li key={option}>Option: {option}</li>)}
      </ol>
      <form onSubmit={onFormSubmit}>
        <input type="text" name="option"/>
        <button>Add Option</button>
      </form>
    </div>
  );

  ReactDOM.render(template, appRoot);
};

renderOptionsChanges();
