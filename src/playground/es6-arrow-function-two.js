// arguments object - no longer bound with arrow functions 

const add = (a, b) => a + b;

console.log('add', add(3, 7));

// this keyword - no longer bound

const user = {
  name: 'geo',
  cities: [
    'Aguas',
    'Buenas',
  ],
  printPlacesLived() {
    this.cities.forEach(city => {
      console.log(this.name + ' has lived in ' + city);
    })
  }
};

user.printPlacesLived();

const userTwo = {
  name: 'geo',
  cities: [
    'Aguas',
    'Buenas',
    'SP',
    'Guada'
  ],
  printPlacesLived() {
    return this.cities.map(city => this.name + ' has lived in ' + city)
  }
};

console.log(userTwo.printPlacesLived());


// challenge

const multiplier = {
  numbers: [2,3,4,5,67,8,9,1,0,23],
  multiplyBy: 3,
  multiply() {
    return this.numbers.map(n => n * this.multiplyBy);
  }
};

console.log(multiplier.multiply());
