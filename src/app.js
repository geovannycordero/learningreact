class IndecisionApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      options: []
    }

    this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
    this.handlePick = this.handlePick.bind(this);
    this.handleAddOption = this.handleAddOption.bind(this);
  }

  handleDeleteOptions() {
    this.setState(() => {
      return {
        options: []
      };
    });
  }

  handlePick() {
    const randomNum = Math.floor(Math.random() * this.state.options.length);
    const optionSelected = this.state.options[randomNum];
    alert('Option selected: ' + optionSelected);
  }

  handleAddOption(option) {
    if(!option) {
      return 'Enter a valid value to add option';
    } else if(this.state.options.indexOf(option) > -1) {
      return 'This option already exist';
    }

    this.setState(prevState => {
      return {
        options: prevState.options.concat(option)
      };
    });
  }

  render() {
    const title = 'Indecision';
    const subtitle = 'Put your life in the hands of a computer';
    const options = ['one', 'two', 'three'];

    return (
      <div>
        <Header title={title} subtitle={subtitle}/>
        <Action
          hasOptions={this.state.options.length > 0}
          handlePick={this.handlePick}
        />
        <Options
          options={this.state.options}
          handleDeleteOptions={this.handleDeleteOptions}
        />
        <AddOption handleAddOption={this.handleAddOption} />
      </div>
    );
  }
}

const Header = props => {
  return (
    <div>
      <h1>{this.props.title}</h1>
      <h2>{this.props.subtitle}</h2>
    </div>
  );
};

const Action = props => {
  return (
    <div>
      <button onClick={this.props.handlePick} disabled={!this.props.hasOptions}>
      What should I do?
      </button>
    </div>
  );
};

const Options = props => {
  return (
    <div>
      {this.props.options.map(
        option => <Option key={option} optionText={option} />
      )}
      <button onClick={this.props.handleDeleteOptions}>Remove All</button>
    </div>
  );
};

class Option extends React.Component {
  render() {
    return (
      <div>
        Option: {this.props.optionText}
      </div>
    )
  }
}

class AddOption extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error: undefined
    };

    this.handleAddOption = this.handleAddOption.bind(this);
  }

  handleAddOption(e) {
    e.preventDefault();

    const value = e.target.elements.option.value.trim();
    const error = this.props.handleAddOption(value);
    e.target.elements.option.value = '';

    this.setState(() => {
      return { error };
    });
  }

  render() {
    return (
      <div>
      {this.state.error && <p>{this.state.error}</p>}
      <form onSubmit={this.handleAddOption}>
        <input type="text" name="option"/>
        <button>Add Option</button>
      </form>
      </div>
    );
  }
}

ReactDOM.render(<IndecisionApp/>, document.getElementById('app'));
