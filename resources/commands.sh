#!/bin/bash

# Install yarn
$ sudo npm i -g yarn

# Install live-server
$ sudo npm i -g live-server

# Create the package.json file
$ yarn init

# Install babel-cli
$ sudo npm i -g babel-cli

# Add babel dependencies to the project
$ yarn add babel-preset-react babel-preset-env

# Translate the JSX code to JS
$ babel src/app.js --out-file=public/scripts/app.js --presets=env,react

# Translate the JSX code to JS and keep it running to automatically change the destination file
$ babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch

# Run the local server
$ live-server public
